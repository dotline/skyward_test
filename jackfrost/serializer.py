from django.contrib.auth.models import User, Group
from jackfrost.models import CurrentConditions, WeatherHistory
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class WeatherSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CurrentConditions
        fields = ('url','location','observation_time',
                    'temp_f','wind_string','url_history')

class WeatherHistorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WeatherHistory
        fields = ('url', 'date', 'temp_f' )
