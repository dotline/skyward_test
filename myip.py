#!python3
import subprocess

std = subprocess.Popen(["ip","-4","-br","add"], stdout=subprocess.PIPE).communicate()[0]
localip = [line.split()[2][:-3] for line in std.decode("utf-8").split("\n") if "UP" in line]


print(localip[0])
